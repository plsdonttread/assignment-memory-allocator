#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <string.h>
#include <errno.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define COLOR_PASS "\033[0;32m" //green
#define COLOR_FAIL "\033[0;31m" //red
#define COLOR_INFO "\033[0;33m" //orange
#define COLOR_DEFAULT "\033[0m"

static void print_msg(char* color, char* test_name, char* msg) {
    printf("%s",color);
    printf("TEST %s: %s\n", test_name, msg);
    printf(COLOR_DEFAULT);
}

static void _debug_heap( FILE* f,  void const* ptr ) {
    bool verbose = false;
    #ifdef VERBOSE
    verbose = true;
    #endif
    if(verbose) debug_heap(f, ptr);
}

static void try_access(void* addr, size_t len) {
    for(size_t i = 0; i < len; i++) {
        ((char*)addr)[i] = '0';
    }
}

static bool check_block(void* contents, size_t len, void* next_contents) {
    try_access(contents, len);
    struct block_header* block = block_get_header(contents);
    struct block_header* next = 
        (next_contents == NULL ? NULL : block_get_header(next_contents));

    bool next_okay = false;
    if(next == NULL && block->next == NULL) next_okay = true;
    else if(next == NULL && block->next->is_free) next_okay = true;
    else next_okay = block->next == next;

    
    return (block->is_free == false) &&
            (block->capacity.bytes >= len) &&
            next_okay;
}

static inline void assert_true(bool value) {if(!value) exit(1);}
static inline void assert_false(bool value) {if(value) exit(1);}

static size_t count_blocks(struct block_header* block) {
    size_t count = 0;
    while(block != NULL) { count++; block = block->next;}
    return count;
}

static size_t count_alloced(struct block_header* block) {
    size_t count = 0;
    while(block != NULL) { if(!block->is_free)count++; block = block->next;}
    return count;
}

static bool test_malloc() {
    const size_t HEAP_SIZE = REGION_MIN_SIZE; //to default size of 2 pages (2*4096)
    const size_t BLOCK_SIZE = 5000;

    struct block_header* heap_start = heap_init(HEAP_SIZE);
    void* p1 = _malloc(BLOCK_SIZE);
    _debug_heap(stdout, heap_start);

    assert_true(check_block(p1, BLOCK_SIZE, NULL));

    assert_true(count_blocks(heap_start) == 2);
    assert_true(count_alloced(heap_start) == 1);
    return true;
}

static bool test_free_one() {
    const size_t HEAP_SIZE = REGION_MIN_SIZE; //to default size of 2 pages (2*4096)
    const size_t BLOCK_SIZE_ONE = 100;
    const size_t BLOCK_SIZE_TWO = 200;
    const size_t BLOCK_SIZE_THREE = 300;

    struct block_header* heap_start = heap_init(HEAP_SIZE);

    void* p1 = _malloc(BLOCK_SIZE_ONE); 
    void* p2 = _malloc(BLOCK_SIZE_TWO);
    void* p3 = _malloc(BLOCK_SIZE_THREE);
    _debug_heap(stdout, heap_start);

    assert_true(check_block(p1, BLOCK_SIZE_ONE, p2));
    assert_true(check_block(p2, BLOCK_SIZE_TWO, p3));
    assert_true(check_block(p3, BLOCK_SIZE_THREE, NULL));

    assert_true(count_blocks(heap_start) == 4);
    assert_true(count_alloced(heap_start) == 3);

    _debug_heap(stdout, heap_start);

    _free(p2);

    assert_true(count_blocks(heap_start) == 4);
    assert_true(count_alloced(heap_start) == 2);
    return true;
}

static bool test_free_two() {
    const size_t HEAP_SIZE = REGION_MIN_SIZE; //to default size of 2 pages (2*4096)
    const size_t BLOCK_SIZE_ONE = 100;
    const size_t BLOCK_SIZE_TWO = 200;
    const size_t BLOCK_SIZE_THREE = 300;
    const size_t BLOCK_SIZE_FOUR = 400;

    struct block_header* heap_start = heap_init(HEAP_SIZE);

    void* p1 = _malloc(BLOCK_SIZE_ONE); 
    void* p2 = _malloc(BLOCK_SIZE_TWO);
    void* p3 = _malloc(BLOCK_SIZE_THREE); 
    void* p4 = _malloc(BLOCK_SIZE_FOUR);

    _debug_heap(stdout, heap_start);

    assert_true(check_block(p1, BLOCK_SIZE_ONE, p2));
    assert_true(check_block(p2, BLOCK_SIZE_TWO, p3));
    assert_true(check_block(p3, BLOCK_SIZE_THREE, p4));
    assert_true(check_block(p4, BLOCK_SIZE_FOUR, NULL));

    assert_true(count_blocks(heap_start) == 5);
    assert_true(count_alloced(heap_start) == 4);

    _free(p3);
    _free(p2);

    _debug_heap(stdout, heap_start);

    assert_true(count_blocks(heap_start) == 4);
    assert_true(count_alloced(heap_start) == 2);
    return true;
}

static bool test_grow_heap_cont() {
    const size_t HEAP_SIZE = REGION_MIN_SIZE; //to default size of 2 pages (2*4096)
    const size_t BLOCK_SIZE_ONE = 4000;
    const size_t BLOCK_SIZE_TWO = 4000;
    const size_t BLOCK_SIZE_THREE = 4000;

    struct block_header* heap_start = heap_init(HEAP_SIZE);

    void* p1 = _malloc(BLOCK_SIZE_ONE); 
    void* p2 = _malloc(BLOCK_SIZE_TWO);
    _debug_heap(stdout, heap_start);
    void* p3 = _malloc(BLOCK_SIZE_THREE); 
    _debug_heap(stdout, heap_start);

    assert_true(check_block(p1, BLOCK_SIZE_ONE, p2));
    assert_true(check_block(p2, BLOCK_SIZE_TWO, p3));
    assert_true(check_block(p3, BLOCK_SIZE_THREE, NULL));

    assert_true(count_blocks(heap_start) == 4);
    assert_true(count_alloced(heap_start) == 3);
    return true;
}

static bool test_grow_heap_spaced() {
    const size_t HEAP_SIZE = REGION_MIN_SIZE; //to default size of 2 pages (2*4096)
    const size_t BLOCK_SIZE_ONE = 3000;
    const size_t BLOCK_SIZE_TWO = 3000;
    const size_t BLOCK_SIZE_THREE = 7000;

    struct block_header* heap_start = heap_init(HEAP_SIZE);

    size_t reg_sz = size_from_capacity(heap_start->capacity).bytes;
    void* dirty_addr = (char*)heap_start + reg_sz;

    #define MAP_ANONYMOUS 0x20
    #define MAP_FIXED_NOREPLACE 0x100000
    mmap(dirty_addr, 1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, 0, 0 );
    
    #ifdef VERBOSE
    char mmap_msg_str[100];
    snprintf(mmap_msg_str, 100, "try mmap dirty: %s", strerror(errno));
    print_msg(COLOR_INFO, "", mmap_msg_str);
    #endif

    void* p1 = _malloc(BLOCK_SIZE_ONE); 
    void* p2 = _malloc(BLOCK_SIZE_TWO);
    _debug_heap(stdout, heap_start);
    void* p3 = _malloc(BLOCK_SIZE_THREE); 
    _debug_heap(stdout, heap_start);

    assert_true(check_block(p1, BLOCK_SIZE_ONE, p2));
    assert_false(check_block(p2, BLOCK_SIZE_TWO, p3));
    assert_true(check_block(p3, BLOCK_SIZE_THREE, NULL));

    assert_true(count_blocks(heap_start) == 5);
    assert_true(count_alloced(heap_start) == 3);
    return true;
}

static bool run_test(char* test_name, bool (*fun)(void)) {
    #ifdef VERBOSE
    print_msg(COLOR_INFO, test_name, "begins");
    #endif
    pid_t pid = fork();
    if(pid == 0) {
        if(!fun()) exit(1);
        exit(0);
    } else {
        int status;
        waitpid(pid, &status, 0);
        if(WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            print_msg(COLOR_PASS, test_name, "passed");
            return true;
        } else {
            print_msg(COLOR_FAIL, test_name, "failed");
            return false;
        }
    }
}

void test_all() {
    run_test("[1] simple malloc", test_malloc);
    run_test("[2] free one block", test_free_one);
    run_test("[3] free two blocks", test_free_two); 
    run_test("[4] grow heap cont", test_grow_heap_cont); 
    run_test("[5] grow heap spaced", test_grow_heap_spaced);
}
